class User < ApplicationRecord
  has_secure_password
  validates :nickname, presence: true, uniqueness: { case_sensetive: false }
  validates_format_of :nickname, :with =>  /[a-z]([a-z][0-9])*/i
  validates :firstname, presence: true
  validates_format_of :firstname, :with => /\A[а-яё]+\z/i
  validates :lastname, presence: true
  validates_format_of :lastname, :with => /\A[а-яё]+\z/i
  validates :email, presence: true, uniqueness: { case_sensetive: false }
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates :password, presence: true
  validates :password, length: { minimum: 5 }
end
