// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready(function () {

  //nickname validation
  $("input#user_nickname").keyup(function(){
    var value = $("input#user_nickname").val();
    var res = /^[a-z]([a-z][0-9])*/ig.exec(value);
    if (res  == null || value.length < 1) {
      $('td.nickname-error').text('Начинается с английской буквы, потом допустимы числа. Один символ минимум.');
      $('input.submit-button').attr('disabled', true);
      $('td.nickname-icon').html('<img src="/assets/red-icon.jpg">');
    } else {
      // check user on server
      $.post('/check_unique_user', { nickname: value });
    }
  });

  //firstname validation
  $("input#user_firstname").keyup(function(){
    var value = $("input#user_firstname").val();
    var res = /[^а-яё]/ig.exec(value);
    if (res || value.length < 1) {
      $('td.firstname-error').text('Допустимы только русские буквы. Один символ минимум.');
      $('input.submit-button').attr('disabled', true);
      $('td.firstname-icon').html('<img src="/assets/red-icon.jpg">');
    } else {
      $('td.firstname-error').text('');
      $('td.firstname-icon').html('<img src="/assets/galka.jpg">');
      unblockButton();
    }
  });

  //lastname validation
  $("input#user_lastname").keyup(function(){
    var value = $("input#user_lastname").val();
    var res = /[^а-яё]/ig.exec(value);
    console.log();
    if (res || value.length < 1) {
      $('td.lastname-error').text('Допустимы только русские буквы. Один символ минимум.');
      $('input.submit-button').attr('disabled', true);
      $('td.lastname-icon').html('<img src="/assets/red-icon.jpg">');
    } else {
      $('td.lastname-error').text('');
      $('td.lastname-icon').html('<img src="/assets/galka.jpg">');
      unblockButton();
    }
  });


  // email validation
  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  $("input#user_email").keyup(function() {
    var value = $("input#user_email").val();
    if (!validateEmail(value)) {
      $('td.email-error').text('Неверный формат электронного адреса');
      $('input.submit-button').attr('disabled', true);
      $('td.email-icon').html('<img src="/assets/red-icon.jpg">');
    } else {
      // check this email on server
      $.post('/check_unique_email', { email: value });
    }
  });

  // password validation
  $("input#user_password").keyup(function() {
    var value = $("input#user_password").val();
    if (value.length < 5) {
      $('td.password-error').text('Пожалуйста, выдумайте пароль длиннее 4 символов.');
      $('input.submit-button').attr('disabled', true);
      $('td.password-icon').html('<img src="/assets/red-icon.jpg">');
    } else {
      $('td.password-error').text('');
      $('td.password-icon').html('<img src="/assets/galka.jpg">');
      unblockButton();
    }
  });

  /* подсчет галок для разблокировки кнопки, ничего умнее сходу не придумал*/
  window.countGalkas = function () {
    var galka = 0;
    $("td.icon img").each(function () {
        if ($(this).attr('src') == '/assets/galka.jpg') {
          galka++;
        }
    });
    return galka;
  };

  window.unblockButton = function () {
    if (countGalkas() == 5) {
      $('input.submit-button').removeAttr('disabled');
    } else {
      $('input.submit-button').attr('disabled', true);
    }
  }

});
