class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy], except: [:check_unique_user, :check_unique_email]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  def check_unique_user
    @user = User.find_by nickname: params[:nickname]
    @result = if @user.nil? then 'uniq' else 'notuniq' end
    respond_to do |format|
      format.js { render :file => "users/check_unique_user.js.erb" }
    end
  end

  def check_unique_email
    @user = User.find_by email: params[:email]
    @result = if @user.nil? then 'uniq' else 'notuniq' end
    respond_to do |format|
      format.js { render :file => "users/check_unique_email.js.erb" }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  #def edit
  #end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'Регистрация завершена.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
=begin
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
=end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:nickname, :firstname, :lastname, :email, :password, :password_confirmation)
    end
end
