Rails.application.routes.draw do
  resources :users, except: [:edit, :update]
  root 'users#new'
  post "/check_unique_user" => "users#check_unique_user", :as => :check_unique_user
  post "/check_unique_email" => "users#check_unique_email", :as => :check_unique_email
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
